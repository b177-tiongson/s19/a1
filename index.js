
let num = prompt("Enter a number: ")
let getCube =  num **3
console.log(`The cube of ${num} is ${getCube}`)

let address = ["258","Washington Ave","NW","California","90011"];

let [numberAddress, avenue, direction, state, zipCode] = address

console.log(`I live in ${numberAddress} ${avenue} ${direction}, ${state} ${zipCode}`)

let animal = {
	name: "Lolong",
	kind: "crocodile",
	habitat: "saltwater",
	weight: "1075 kg",
	measurement: "20 ft 3in"
}

let {name,kind,habitat,weight,measurement} = animal

console.log(`${name} was a ${habitat} ${kind}. He weighted at ${weight} with a measurement of ${measurement}.`)

let numbers = [1,2,3,4,5]

numbers.forEach((number) => console.log(`${number}`))

let reduceNumber = numbers.reduce((x,y) => x+y)

console.log(reduceNumber)

class Dog{
	constructor(name, age, breed){
		this.name = name
		this.age = age
		this.breed = breed
	}
}

let myDog = new Dog("Frankie",5,"Miniature Duchshund");

console.log(myDog)












